package game

import (
	"math/rand"
)

const (
	empty   = iota
	crosses = iota
	noughts = iota
)

type Games struct {
	All    map[int]Game
	nextId int
}

type Game struct {
	Turn   int
	Board  [][]int
	Winner *int
}

type DescriptiveGame struct {
	Turn   string
	Board  [][]string
	Winner *string
}

type Move struct {
    Player string `json:player`
    X int `json:x`
    Y int `json:y`
}

func NewBoard() [][]int {
	board := make([][]int, 3)
	for y := 0; y < 3; y++ {
		board[y] = make([]int, 3)
	    for x := 0; x < 3; x++ {
		    board[y][x] = empty
        }
	}
    return board
}

func newGame() Game {
	turn := rand.Intn(2) + 1
	return Game{Turn: turn, Board: NewBoard(), Winner: nil}
}

func NewGames() Games {
	return Games{All: make(map[int]Game), nextId: 1}
}

func (gs *Games) NewGame() Game {
	game := newGame()
	gs.All[gs.nextId] = game
	gs.nextId++
	return game
}

type InvalidPlayerError struct {}

func(ip InvalidPlayerError) Error() string{
    return "Invalid Player"
}

type GameOverError struct {}

func(goe GameOverError) Error() string{
    return "Game Over"
}

type NotYourTurnError struct {}

func(nyte NotYourTurnError) Error() string{
    return "Not your turn"
}

func getPlayer(player string) (int, error) {
    if player == "X" {
        return crosses, nil
    } else if player == "O" {
        return noughts, nil
    } else {
        return empty, InvalidPlayerError{}
    }
}

func GetPlayerDescription(player int) string {
    if player == crosses {
        return "X"
    } else if player == noughts {
        return "O"
    } else {
        return "-"
    }
}

func (g *Game) MakeMove(move Move) error {
    if g.Winner != nil {
        return GameOverError{}
    }
    intPlayer, err := getPlayer(move.Player)
    if err != nil {
        return err
    }
    if intPlayer != g.Turn {
        return NotYourTurnError{}
    }

    if(g.Board[move.Y][move.X] != empty) {
        return AlreadyOccupiedError{}
    }
    g.Board[move.Y][move.X] = intPlayer
    if intPlayer == crosses {
        g.Turn = noughts
    } else {
        g.Turn = crosses
    }
    g.UpdateWin()
    return nil
}

func (g *Game) UpdateWin() {
    // handle diagonal wins
    if g.Board[1][1] != empty {
        if g.Board[0][0] == g.Board[1][1] && g.Board[1][1] == g.Board[2][2] {
            g.Winner = &g.Board[1][1]
            return
        } else if g.Board[0][2] == g.Board[1][1] && g.Board[1][1] == g.Board[2][0] {
            g.Winner = &g.Board[1][1]
            return
        }
    }

    for i := 0; i < 3; i++ {
        // horizontal
        if g.Board[0][i] != empty && g.Board[0][i] == g.Board[1][i] && g.Board[1][i] == g.Board[2][i] {
            g.Winner = &g.Board[0][i]
            return
        }

        // vertical
        if g.Board[i][0] != empty && g.Board[i][0] == g.Board[i][1] && g.Board[i][1] == g.Board[i][2] {
            g.Winner = &g.Board[i][0]
            return
        }
    }
}

func (gs *Games) ToDescriptiveGames() map[int]DescriptiveGame{
    descriptiveGames := make(map[int]DescriptiveGame)
    for key, game := range gs.All {
        descriptiveGames[key] = game.ToDescriptiveGame()
    }
    return descriptiveGames
}

func (g *Game) ToDescriptiveGame() DescriptiveGame {
    board := make([][]string, 0)
    for _, row := range g.Board {
        newRow := make([]string, 0)
        for _, cell := range row {
            newRow = append(newRow, GetPlayerDescription(cell))
        }
        board = append(board, newRow)
    }
    var winner *string
    if g.Winner == nil {
        winner = nil
    } else {
        desc := GetPlayerDescription(*g.Winner)
        winner = &desc
    }
    return DescriptiveGame{Turn: GetPlayerDescription(g.Turn), Board: board, Winner: winner}
}

type AlreadyOccupiedError struct {}
func(aoe AlreadyOccupiedError) Error() string {
    return "Already Occupied"
}

