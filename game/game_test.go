package game

import (
    "testing"
    "xo/game"
)

func TestNewBoard(t *testing.T) {
    board := game.NewBoard()
    for _, row := range board {
        for _, cell := range row {
            if cell != 0 {
                t.Fail()
            }
        }
    }
}

func TestMakeMoveGameOver(t *testing.T) {
    games := game.NewGames()
    g := games.NewGame()
    g.Board = [][]int{
        []int{
            1, 1, 1,
        },
        []int{
            0, 0, 0,
        },
        []int{
            0, 0, 0,
        },
    }
    g.UpdateWin()
    turn := game.GetPlayerDescription(g.Turn)
    err := g.MakeMove(game.Move{turn, 1, 1})
    gameOver := game.GameOverError{}
    if err != gameOver{
        t.Fail()
    }
}

func TestMakeMoveNotYourTurn(t *testing.T) {
    games := game.NewGames()
    g := games.NewGame()

    var turn string
    if g.Turn == 1 {
        turn = game.GetPlayerDescription(2)
    } else {
        turn = game.GetPlayerDescription(1)
    }

    err := g.MakeMove(game.Move{turn, 1, 1})
    gameOver := game.NotYourTurnError{}
    if err != gameOver{
        t.Fail()
    }
}

func TestMakeMoveAlreadyOccupied(t *testing.T) {
    games := game.NewGames()
    g := games.NewGame()
    g.Board = [][]int{
        []int{
            1, 1, 0,
        },
        []int{
            0, 0, 0,
        },
        []int{
            0, 0, 0,
        },
    }
    turn := game.GetPlayerDescription(g.Turn)
    err := g.MakeMove(game.Move{turn, 0, 0})
    gameOver := game.AlreadyOccupiedError{}
    if err != gameOver{
        t.Fail()
    }
}

func TestUpdateWinRow(t *testing.T) {
    games := game.NewGames()
    g := games.NewGame()
    g.Board = [][]int{
        []int{
            1, 1, 1,
        },
        []int{
            0, 0, 0,
        },
        []int{
            0, 0, 0,
        },
    }
    g.UpdateWin()
    if *g.Winner != 1{
        t.Fail()
    }
}

func TestUpdateWinCol(t *testing.T) {
    games := game.NewGames()
    g := games.NewGame()
    g.Board = [][]int{
        []int{
            1, 0, 0,
        },
        []int{
            1, 0, 0,
        },
        []int{
            1, 0, 0,
        },
    }
    g.UpdateWin()
    if *g.Winner != 1 {
        t.Fail()
    }
}

func TestUpdateWinDiag1(t *testing.T) {
    games := game.NewGames()
    g := games.NewGame()
    g.Board = [][]int{
        []int{
            1, 0, 0,
        },
        []int{
            0, 1, 0,
        },
        []int{
            0, 0, 1,
        },
    }
    g.UpdateWin()
    if *g.Winner != 1 {
        t.Fail()
    }
}

func TestUpdateWinDiag2(t *testing.T) {
    games := game.NewGames()
    g := games.NewGame()
    g.Board = [][]int{
        []int{
            0, 0, 1,
        },
        []int{
            0, 1, 0,
        },
        []int{
            1, 0, 0,
        },
    }
    g.UpdateWin()
    if *g.Winner != 1 {
        t.Fail()
    }
}
