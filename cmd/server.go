package main

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"xo/game"
)

func getErrorMessage(err error) []byte{
    response := make(map[string]string)
    response["error"] = err.Error()
    b, err := json.Marshal(response)
    return b
}

func handleError(err error, statusCode int, w http.ResponseWriter){
    log.Printf("%s", err)
	w.WriteHeader(statusCode)
    w.Write(getErrorMessage(err))
}

type MissingGameError struct {}
func (mge MissingGameError) Error() string {
    return "Missing Game"
}


type Server struct {
	routes map[string]map[string]func(http.ResponseWriter, *http.Request)
	games  game.Games
}

func NewServer() Server {
	routes := make(map[string]map[string]func(http.ResponseWriter, *http.Request))
	return Server{routes: routes, games: game.NewGames()}
}

func (s *Server) AddRoute(path string, method string, handler func(http.ResponseWriter, *http.Request)) {
	if _, exists := s.routes[path]; !exists {
		s.routes[path] = make(map[string]func(http.ResponseWriter, *http.Request))
	}
	s.routes[path][method] = handler
}

func (s *Server) SetupRoutes() {
	genHandler := func(handlers map[string]func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
		return func(w http.ResponseWriter, r *http.Request) {
			log.Printf("%s Request to: %s", r.Method, r.URL)
			if h, exists := handlers[r.Method]; exists {
				h(w, r)
			} else {
				w.WriteHeader(http.StatusMethodNotAllowed)
			}
		}
	}
	for path, methodHandlers := range s.routes {
		http.HandleFunc(path, genHandler(methodHandlers))
	}
}

func (s *Server) getAllRoutes(w http.ResponseWriter, r *http.Request) {
	routes := make(map[string][]string)
	for route, handlers := range s.routes {
		methods := make([]string, 0)
		for method := range handlers {
			methods = append(methods, method)
		}
		routes[route] = methods
	}
	b, err := json.Marshal(routes)
	if err != nil {
        handleError(err, http.StatusBadRequest, w)
	} else {
	    w.Write(b)
    }
}

func (s *Server) getGames(w http.ResponseWriter, r *http.Request) {
	regexp, _ := regexp.Compile("/game/([0-9]+)")
	var games map[int]game.DescriptiveGame
	path := (*r.URL).EscapedPath()
	if match := regexp.FindStringSubmatch(path); len(match) > 0 {
		gameId, _ := strconv.Atoi(match[1])
		games = make(map[int]game.DescriptiveGame)
        if g, exists := s.games.All[gameId]; !exists{
            err := MissingGameError{}
            handleError(err, http.StatusNotFound, w)
            return
        } else {
            games[gameId] = g.ToDescriptiveGame()
        }
	} else {
		games = s.games.ToDescriptiveGames()
	}

	b, err := json.Marshal(games)
	if err != nil {
        handleError(err, http.StatusBadRequest, w)
	} else {
	    w.Write(b)
    }
}

func (s *Server) makeMove(w http.ResponseWriter, r *http.Request){
	regexp, _ := regexp.Compile("/move/([0-9]+)")
	path := (*r.URL).EscapedPath()

    decoder := json.NewDecoder(r.Body)
    var move game.Move
    err := decoder.Decode(&move)
    if err != nil {
        handleError(err, http.StatusBadRequest, w)
        return
    }

	if match := regexp.FindStringSubmatch(path); len(match) > 0 {
		gameId, _ := strconv.Atoi(match[1])
        g, exists := s.games.All[gameId]
        if !exists {
            err = MissingGameError{}
            handleError(err, http.StatusNotFound, w)
            return
        }

        err := g.MakeMove(move)
        if err != nil {
            handleError(err, http.StatusBadRequest, w)
            return
        }
        s.games.All[gameId] = g

        b, err := json.Marshal(g.ToDescriptiveGame())
        if err != nil {
            handleError(err, http.StatusBadRequest, w)
            return
        }
        w.Write(b)
    } else {
        err = MissingGameError{}
        handleError(err, http.StatusNotFound, w)
    }
}

func (s *Server) newGame(w http.ResponseWriter, r *http.Request) {
	game := s.games.NewGame()
	b, err := json.Marshal(game.ToDescriptiveGame())
	if err != nil {
        handleError(err, http.StatusBadRequest, w)
	} else {
	    w.Write(b)
    }
}

func main() {
	server := NewServer()
	server.AddRoute("/", http.MethodGet, server.getAllRoutes)
	server.AddRoute("/game/", http.MethodPost, server.newGame)
	server.AddRoute("/game/", http.MethodGet, server.getGames)
	server.AddRoute("/move/", http.MethodPost, server.makeMove)

	server.SetupRoutes()
	log.Fatal(http.ListenAndServe(":8080", nil))
}
