# Noughts and Crosses API

To run (from this directory):

`go run cmd/server.go`

This will run the server on `localhost:8080`

To run the tests for the game package do:

`go test game/game_test.go`

## API endpoints

### POST /game/
* `curl -X POST localhost:8080/game/`
* Creates a new game

### POST /move/{game_id}: 
* `curl -X POST localhost:8080/move/1 -d '{"player": "O", "x": 1, "y": 2}'` 
* JSON message format: {"user": "X", "x": 0, "y": 0}
* Responses:
    * New game State {"turn": "X", winner: null, board: [["X", "-", "-"], ["O", "-", "-"], ["-", "-", "-"]]}
    * {"error": Error Message}
        Potential errors:
            * "Invalid Player"
            * "Game Over"
            * "Not your turn"
            * "Already Occupied"
            * "Missing Game"

Coordinate system (x, y) for making moves: 
```
(0, 0) | (1, 0) | (2, 0)
------------------------
(0, 1) | (1, 1) | (2, 1)
------------------------
(0, 2) | (1, 2) | (2, 2)
```

### GET /game/{game_id}
* `curl localhost:8080/game/1` 
* {"1": {"turn": "X", winner: null, board: [["X", "-", "-"], ["O", "-", "-"], ["-", "-", "-"]]}}

### GET /game/
* `curl localhost:8080/game/` 
* Response is a list of all games (see above)

### GET /
* `curl localhost:8080/` 
* List all routes


Potential Improvements:
* Proper routing using regex and then move the "move" endpoint into `localhost:8080/game/{id}/move`
* Persist game state in a database so restart during a game doesn't lost it's state
* Allow filtering to only view "in progress" games
* Authentication of users and use username instead of X/O as the user, with the mapping of X/O to user being stored as part of the Game struct
